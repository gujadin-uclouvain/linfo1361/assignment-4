[LINFO1361] IA - Assignment n°4
---

**Membres:**

* Guillaume Jadin
* Thomas Robert


**Description:**

* In this assignment you will design an algorithm to solve the vertex cover problem. You are provided with an integer k and an undirected graph. Your task is to find the set of k vertices that touch the maximal number of edges.

* For the N-Queens Problem, your task is to model this problem with propositional logic. We define n_rows × n_columns variables: Cij is true iff there is a queen on cell at position (i, j); false otherwise. The chessboard origin (0, 0) is at left top corner.


**Restrictions:**

* vertexcover_max.py (INGInious): your maxvalue local search for the Maximum Vertex Cover problem, to submit on INGInious in the Assignment4: Cover Vertex max value task.

* vertexcover_random_max.py (INGInious): your randomized max value local search for the Maximum Vertex Cover problem, to submit on INGInious in the Assignment4: Vertex Cover randomized maxvalue task.

* queen_solver.py (INGInious): which contains get_expression method to solve the Queen problem, to submit on INGInious in the Assignment4: Queen Problem task.

* report_A4_group_XX.pdf (Gradescope): Answers to all the questions using the provided template. Remember, the more concise the answers, the better.


**INGInious:**

* https://inginious.info.ucl.ac.be/course/LINGI2261


**Overleaf:**

* https://www.overleaf.com/6161743332nmbgfvnsnzcv