#! /usr/bin/env python3
"""NAMES OF THE AUTHOR(S): Nicolas Golenvaux <nicolas.golenvaux@uclouvain.be>"""
import time

from search import *
import sys
from copy import copy


class VertexCover(Problem):

    # if you want you can implement this method and use it in the maxvalue and randomized_maxvalue functions
    def successor(self, state):
        for i_node_cover, node_cover in enumerate(state.cover):
            for node_not_cover in state.not_cover:
                new_cover = copy(state.cover)
                new_cover[i_node_cover] = node_not_cover
                yield None, State(state.k, state.vertices, state.edges, new_cover)


    # if you want you can implement this method and use it in the maxvalue and randomized_maxvalue functions
    def value(self, state):
        # print("VALUES =>", state.cover)
        curr_set = set(state.vertices[state.cover[0]])
        for nbr_cover in range(1, len(state.cover)):
            curr_set = curr_set.union(state.vertices[state.cover[nbr_cover]])
        return len(curr_set)


class State:

    def __init__(self, k, vertices, edges, cover=None, not_cover=None):
        self.k = k
        self.n_vertices = len(vertices)
        self.n_edges = len(edges)
        self.vertices = vertices
        self.edges = edges
        if cover is None:
            self.cover = self.build_init()
        else:
            self.cover = cover
        if not_cover is None:
            self.not_cover = [v for v in range(self.n_vertices) if v not in self.cover]
        else:
            self.not_cover = not_cover

    # an init state building is provided here but you can change it at will
    def build_init(self):
        return list(range(self.k))

    def __str__(self):
        s = ''
        for v in self.cover:
            s += ' ' + str(v)
        return s


def read_instance(instanceFile):
    file = open(instanceFile)
    line = file.readline()
    k = int(line.split(' ')[0])
    n_vertices = int(line.split(' ')[1])
    n_edges = int(line.split(' ')[2])
    vertices = {}
    for i in range(n_vertices):
        vertices[i] = []
    edges = {}
    line = file.readline()
    while line:
        [edge, vertex1, vertex2] = [int(x) for x in line.split(' ')]
        vertices[vertex1] += [edge]
        vertices[vertex2] += [edge]
        edges[edge] = (vertex1, vertex2)
        line = file.readline()
    return k, vertices, edges


def sorted_successors(current):
    successors = [(successor, successor.value()) for successor in current.expand()]
    return sorted(successors, key=lambda x: x[1], reverse=True)

# Attention : Depending of the objective function you use, your goal can be to maximize or to minimize it
def maxvalue(problem, limit=100, callback=None, compute_best_step=False):
    current = LSNode(problem, problem.initial, 0)
    best, best_value = current, current.value()
    best_step = 0
    for step in range(limit):
        if callback is not None:
            callback(current)
        current, current_value = sorted_successors(current)[0]
        if current_value > best_value:
            best, best_value = current, current_value
            best_step = step
    return best, best_step if compute_best_step else best


# Attention : Depending of the objective function you use, your goal can be to maximize or to minimize it
def randomized_maxvalue(problem, limit=100, callback=None, compute_best_step=False):
    current = LSNode(problem, problem.initial, 0)
    best = current
    best_step = 0
    for step in range(limit):
        if callback is not None:
            callback(current)
        best5successors = sorted_successors(current)[:5]
        current = random.choice(best5successors)[0]
        if current.value() > best.value():
            best = current
            best_step = step
    return best, best_step if compute_best_step else best


#####################
#       Launch      #
#####################
def iterate_files():
    for i in range(1, 10):
        yield "./instances/i0"+str(i)+".txt"
    yield "./instances/i10.txt"

def print_all_results():
    print("=" * 50)
    print("FILE =>", file)
    for fun, str_fun in [(maxvalue, "maxvalue"), (randomized_maxvalue, "randomized_maxvalue"),
                         (random_walk, "random_walk")]:
        start = time.time()
        node, ns = fun(vc_problem, step_limit, compute_best_step=True)
        end = time.time()
        print("=" * 25)
        print("Function", "\t", str_fun)
        print("T(s)", "\t\t", str(round(end - start, 3)))
        print("Val", "\t\t", str(node.value()))
        print("NS", "\t\t\t", str(ns))
        print("=" * 25)
    print("=" * 50)

if __name__ == '__main__':
    # info = read_instance(sys.argv[1])
    for file in iterate_files():
        info = read_instance(file)
        init_state = State(info[0], info[1], info[2])
        vc_problem = VertexCover(init_state)
        step_limit = 100
        print_all_results()
        # node = maxvalue(vc_problem, step_limit)
        # # node = randomized_maxvalue(vc_problem, step_limit)
        # # node = random_walk(vc_problem, step_limit)
        # state = node.state
        # print(state)
