from clause import *

"""
For the queen problem, the only code you have to do is in this file.

You should replace

# your code here

by a code generating a list of clauses modeling the queen problem
for the input file.

You should build clauses using the Clause class defined in clause.py

Read the comment on top of clause.py to see how this works.
"""

def get_expression(size, queens=None):
    def clause_maker(curr_index, new_index=None, is_negative=True):
        sub_clause = Clause(size)
        if not is_negative:
            sub_clause.add_positive(curr_index[0], curr_index[1])
        elif new_index is not None:
            sub_clause.add_negative(curr_index[0], curr_index[1])
            sub_clause.add_negative(new_index[0], new_index[1])
        else:
            raise ValueError("new_index must have a value with is_negative=True")
        return sub_clause

    expression = []
    if queens is not None:
        # Add Queens already placed from input
        for queen in queens:
            expression.append(clause_maker((queen[0], queen[1]), is_negative=False))

    for row in range(size):
        queens_in_curr_row_clause = Clause(size)
        for col in range(size):
            # There is a queen in index (row, col)
            queens_in_curr_row_clause.add_positive(row, col)
            for k in range(size):
                # Clauses for the queen in index (row, col)
                if k != col:
                    # Not on the same row
                    expression.append(clause_maker((row, col), (row, k)))
                if k != row:
                    # Not on the same column
                    expression.append(clause_maker((row, col), (k, col)))
                if k != 0:
                    # Not on the same diagonal
                    if 0 <= row+k < size and 0 <= col+k < size:
                        # Down-Right
                        expression.append(clause_maker((row, col), (row+k, col+k)))
                    if 0 <= row+k < size and 0 <= col-k < size:
                        # Down-Left
                        expression.append(clause_maker((row, col), (row+k, col-k)))
                    if 0 <= row-k < size and 0 <= col+k < size:
                        # Up-Right
                        expression.append(clause_maker((row, col), (row-k, col+k)))
                    if 0 <= row-k < size and 0 <= col-k < size:
                        # Up-Left
                        expression.append(clause_maker((row, col), (row-k, col-k)))
        expression.append(queens_in_curr_row_clause)

    return expression


if __name__ == '__main__':
    expression = get_expression(3)
    for clause in expression:
        print(clause)
